# rsync

This is a CERN rebuild of the upstream `rsync` package, essentially including the fix https://github.com/WayneD/rsync/pull/204/ which has not been backported into the version of `rsync` in el9
